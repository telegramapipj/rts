FROM --platform=linux/x86_64 ubuntu:22.04

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app

RUN apt update -y
RUN apt install sudo -y && sudo apt install build-essential git python3 python3-pip libssl-dev libboost-all-dev -y
RUN git clone --recursive https://github.com/arvidn/libtorrent
RUN cd libtorrent && python3 setup.py build && python3 setup.py install
RUN cd libtorrent && python3 -m pip install wheel && python3 setup.py bdist_wheel && cd ..

COPY . .

RUN sudo pip3 install --no-cache-dir -r important.txt

CMD ["/usr/bin/python3" "pyTelegramShellBot.py"]