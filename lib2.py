import libtorrent as lt
import time
import sys
import subprocess

def download_and_seed_torrent(torrent_file, save_path):
    ses = lt.session()
    ses.listen_on(6881, 6891)

    info = lt.torrent_info(torrent_file)
    h = ses.add_torrent({
        'ti': info,
        'save_path': save_path,
    })

    print(f"Starting to download: {h.name()}")

    while not h.is_seed():
        s = h.status()
        print(f'Downloading: {s.progress * 100:.2f}% complete (download rate: {s.download_rate / 1000:.2f} kB/s, upload rate: {s.upload_rate / 1000:.2f} kB/s, peers: {s.num_peers})')
        time.sleep(1)

    print(f"Download completed: {h.name()}")

    # Now continue seeding
    print("Seeding... Press Ctrl+C to stop.")
    try:
        while True:
            s = h.status()
            print(f'Seeding: (upload rate: {s.upload_rate / 1000:.2f} kB/s, peers: {s.num_peers})')
            time.sleep(10)  # Adjust the sleep time as needed
    except KeyboardInterrupt:
        print("Seeding stopped by user.")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python download_torrent.py <torrent_file> <save_path>")
        sys.exit(1)

    torrent_file = sys.argv[1]
    save_path = sys.argv[2]
    
    # Start downloading and seeding the torrent
    download_and_seed_torrent(torrent_file, save_path)